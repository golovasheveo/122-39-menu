package telran;

import telran.employee.*;
import telran.menu.*;

import java.time.LocalDate;
import java.util.HashMap;

public class EmployeesMenuAppl {
    static InputOutput inputOutput = new ConsoleInputOutput();


    public static void main(String[] args) {
        Employee emp1 =
                new Employee("email1@gmail.com", "0531111111",
                        LocalDate.parse("2000-01-01"), 1000, "Developer");
        Employee emp2 =
                new Employee("email2@gmail.com", "0531111112",
                        LocalDate.parse("2000-01-02"), 2000, "QA_Tester");
        Employee emp3 =
                new Employee("email3@gmail.com", "0531111113",
                        LocalDate.parse("2000-01-03"), 3000, "Development_Manager");
        Employee emp4 =
                new Employee("email4@gmail.com", "0531111114",
                        LocalDate.parse("2000-01-04"), 4000, "QA_Manager");

        HashMap<String, Employee> employees = new HashMap<>();

        employees.put(emp1.getEmail(), emp1);
        employees.put(emp2.getEmail(), emp2);
        employees.put(emp3.getEmail(), emp3);
        employees.put(emp4.getEmail(), emp4);


        Item[] items = {
                new DisplayAllEmployeesItem(employees, inputOutput),
                new AddEmployeeItem(employees, inputOutput),
                new DisplayEmployeeItem(employees, inputOutput),
                new RemoveEmployeeItem(employees, inputOutput),
                new ExitItem()
        };

        Menu menu = new Menu(items, inputOutput);
        menu.menuRun();
    }

}
