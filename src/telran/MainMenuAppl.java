package telran;

import telran.chkitems.DateItem;
import telran.chkitems.EmailItem;
import telran.chkitems.IPv4Item;
import telran.chkitems.PhoneNumberItem;
import telran.menu.*;

public class MainMenuAppl {
    static InputOutput inputOutput = new ConsoleInputOutput();

    public static void main(String[] args) {
        Item[] items = {
                new PhoneNumberItem(inputOutput),
                new IPv4Item(inputOutput),
                new EmailItem(inputOutput),
                new DateItem(inputOutput),
                new ExitItem()
        };

        Menu menu = new Menu(items, inputOutput);
        menu.menuRun();
    }

}
