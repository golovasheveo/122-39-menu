package telran.employee;

import telran.menu.InputOutput;

import java.util.HashMap;

public class DisplayAllEmployeesItem extends AbstractEmployeesItem {
    private final InputOutput inputOutput;

    public DisplayAllEmployeesItem(HashMap<String, Employee> employees, InputOutput inputOutput) {
        super(employees, inputOutput);
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Display all employees";
    }

    @Override
    public void perform() {
        if (employees.size() == 0) {
            inputOutput.displayLine("No employees found\n");
        } else
            employees.keySet().stream()
                    .map(emailKey -> emailKey + ": " + employees.get(emailKey)).forEach(inputOutput::displayLine);
    }

}
