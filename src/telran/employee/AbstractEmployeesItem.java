package telran.employee;

import telran.menu.InputOutput;
import telran.menu.Item;

import java.util.HashMap;

public abstract class AbstractEmployeesItem implements Item {
    protected HashMap<String, Employee> employees;

    public AbstractEmployeesItem(HashMap<String, Employee> employees, InputOutput inputOutput) {
        this.employees = employees;

    }

}
