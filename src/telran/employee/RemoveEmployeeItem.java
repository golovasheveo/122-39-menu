package telran.employee;

import telran.menu.InputOutput;

import java.util.HashMap;

public class RemoveEmployeeItem extends AbstractEmployeesItem {
    InputOutput io;

    public RemoveEmployeeItem(HashMap<String, Employee> employees, InputOutput io) {
        super(employees, io);
        this.io = io;
    }


    @Override
    public String displayName() {
        return "Remove employee";
    }

    @Override
    public void perform() {
        String email = io.inputEmail("Enter email for remove");

        if (employees.containsKey(email)) {
            Employee removedEmployee = employees.get(email);
            employees.remove(email);
            io.displayLine(removedEmployee + " removed successfully \n");
        } else {
            io.displayLine("Employee with this email doesn't found\n");
        }


    }

}
