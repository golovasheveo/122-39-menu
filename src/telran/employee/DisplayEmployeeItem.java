package telran.employee;

import telran.menu.InputOutput;

import java.util.HashMap;

public class DisplayEmployeeItem extends AbstractEmployeesItem {
    InputOutput inputOutput;

    public DisplayEmployeeItem(HashMap<String, Employee> employees, InputOutput inputOutput) {
        super(employees, inputOutput);
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Display Employee Item";
    }

    @Override
    public void perform() {
        String email = inputOutput.inputEmail("Enter email for employee");
        inputOutput.displayLine(employees.containsKey(email) ? employees.get(email) + "\n" : "Employee not found\n");
    }

}
