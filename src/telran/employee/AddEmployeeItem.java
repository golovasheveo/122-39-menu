package telran.employee;

import telran.menu.InputOutput;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;

public class AddEmployeeItem extends AbstractEmployeesItem {
    private final InputOutput inputOutput;

    public AddEmployeeItem(HashMap<String, Employee> employees, InputOutput inputOutput) {
        super(employees, inputOutput);
        this.inputOutput = inputOutput;
    }

    private HashSet<String> getTitle() {
        HashSet<String> res = new HashSet<>();
        res.add("Developer");
        res.add("QA_Tester");
        res.add("Development_Manager");
        res.add("QA_Manager");
        return res;
    }

    @Override
    public String displayName() {
        return "Add Employee";
    }

    @Override
    public void perform() {

        String email = inputOutput.inputEmail("Enter Employee email");
        String phone = inputOutput.inputPhoneNumber("Enter Phone Number");
        LocalDate birthDate = inputOutput.inputDate("Enter birth date in format YYYY-MM-DD");
        int salary = inputOutput.inputInteger("Enter salary");
        String title = inputOutput.inputOptions("Enter Position", getTitle());

        if (employees.containsKey(email)) {
            inputOutput.displayLine("Item already exists\n");
        } else {
            employees.put(email, new Employee(email, phone, birthDate, salary, title));
            inputOutput.displayLine("Added\n");
        }

    }

}
