package telran.menu;

import java.time.LocalDate;
import java.util.Set;
import java.util.function.Function;

public interface InputOutput {
    String inputString(String prompt);

    void display(String obj);

    default <T> T inputObject(String prompt, String errorMessage, Function<String, T> mapper) {
        T res;
        while (true) {
            String str = inputString(prompt);
            try {
                res = mapper.apply(str);
                if (res != null) {
                    break;
                }
            } catch (Exception e) {

            }
            displayLine(errorMessage);
        }
        return res;
    }

    default Integer inputInteger(String prompt, Integer min, Integer max) {

        String message = String.format("The number not in range %d - %d", min, max);
        return inputObject(prompt, message, s -> {
            Integer res = Integer.parseInt(s);
            return res >= min && res <= max ? res : null;
        });
    }

    default void displayLine(Object obj) {
        display(obj.toString() + "\n");
    }

    default Integer inputInteger(String prompt) {
        String message = "number not correct";
        return inputObject(prompt, message, Integer::parseInt);
    }

    default String inputOptions(String prompt, Set<String> options) {
        String message = "value not correct";
        return inputObject(prompt, message, e -> options.contains(e) ? e : null);
    }

    default LocalDate inputDate(String prompt) {

        String message = "Please, enter date in format YYYY-MM-DD";

        return inputObject(prompt, message, LocalDate::parse);
    }

    default String inputEmail(String prompt) {

        String message = "Please enter correct email";

        return inputObject(prompt, message, e -> e.matches(RegularExpression.email()) ? e : null);
    }

    default String inputPhoneNumber(String prompt) {
        String message = "Please enter correct phone number";
        return inputObject(prompt, message, e -> e.matches(RegularExpression.chkIsraelNumber()) ? e : null);
    }

    default String inputIpV4(String prompt) {
        String message = "Please enter correct Ipv4";
        return inputObject(prompt, message, e -> e.matches(RegularExpression.ipV4()) ? e : null);
    }
}
