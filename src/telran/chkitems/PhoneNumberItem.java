package telran.chkitems;

import telran.menu.InputOutput;
import telran.menu.Item;

public class PhoneNumberItem implements Item {
    InputOutput inputOutput;

    public PhoneNumberItem(InputOutput inputOutput) {
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Check Israel phone ";
    }

    @Override
    public void perform() {
        String res = inputOutput.inputPhoneNumber("Enter phone number");
        inputOutput.displayLine(res + " - this is your phone");
    }

}
