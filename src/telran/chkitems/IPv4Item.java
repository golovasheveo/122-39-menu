package telran.chkitems;

import telran.menu.InputOutput;
import telran.menu.Item;

public class IPv4Item implements Item {
    InputOutput inputOutput;

    public IPv4Item(InputOutput inputOutput) {
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Check IPv4";
    }

    @Override
    public void perform() {
        String res = inputOutput.inputIpV4("Enter IP");
        inputOutput.displayLine(res + " - this is your IPv4");
    }
}
