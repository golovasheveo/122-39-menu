package telran.chkitems;

import telran.menu.InputOutput;
import telran.menu.Item;

public class EmailItem implements Item {
    InputOutput inputOutput;

    public EmailItem(InputOutput inputOutput) {
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Check email";
    }

    @Override
    public void perform() {
        String res = inputOutput.inputEmail("Enter Email");
        inputOutput.displayLine(res + " - this is your Email");
    }

}
