package telran.chkitems;

import telran.menu.InputOutput;
import telran.menu.Item;

import java.time.LocalDate;

public class DateItem implements Item {
    InputOutput inputOutput;

    public DateItem(InputOutput inputOutput) {
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Check date";
    }

    @Override
    public void perform() {
        LocalDate date = inputOutput.inputDate("Input date in format YYYY-MM-DD");
        inputOutput.displayLine(date);

    }

}
